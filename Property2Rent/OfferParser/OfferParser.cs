﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using AutoMapper;
using CsvHelper;
using CsvHelper.Configuration;
using Property2Rent.Enums;
using Property2Rent.Extensions;
using Property2Rent.Models;
using Property2Rent.Profiles;
using Property2Rent.Utils;

namespace Property2Rent.OfferParser
{
    public class OfferParser
    {
        private readonly IMapper mapper;
        private string selectedFile = string.Empty;
        private readonly CultureInfo customCulture;
        public IList<OfferModel> Data { get; private set; }

        public OfferParser(IMapper mapper)
        {
            customCulture = new CultureInfo("en-US");
            this.mapper = mapper;
        }

        public void LoadData()
        {
            // SetPathToFile();
            var data = DataTransfer.Instance.GetCsvData().Result;
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                NewLine = Environment.NewLine,
                HasHeaderRecord = true
            };

            // using (var text = new StreamReader(selectedFile))
            using (var text = new StringReader(data))
            using (var csv = new CsvReader(text, config))
            {
                csv.Context.RegisterClassMap<OfferMap>();
                Data = csv.GetRecords<OfferModel>().Select(x =>
                {
                    if (x.Id.Equals(Guid.Empty))
                    {
                        x.Id = Guid.NewGuid();
                    }
                    return x;
                }).ToList();
            }
        }

        public void BiggestResidential()
        {
            var properties = (from data in Data group data by data.City into g1 select g1)
                .SelectMany(x => x.Where(y => y.Area == x.Max(z => z.Area) && y.PropertyType.Equals(PropertyType.Residential.GetDescription())));
            var collection = Data;
            FullReport(collection);
        }

        public void MostExpensiveResidential()
        {
            var properties = (from cities in Data
                group cities by cities.City into g1
                select g1).SelectMany(x => x.Where(y => y.Price == x.Max(z => z.Price)));

            var collection = Data.Join(properties, d => d.City, p => p.City, (d,p) => new { Left = d, Right = p})
                .GroupBy(x => x.Left.City)
                .Select(x => x.First().Right)
                .OrderBy(x => x.City);
            var withTaxes = mapper.Map<IList<ShortOfferModel>>(collection);
            ShortReport(withTaxes);
        }

        public void CheapestProperties()
        {
            Console.Clear();
            var data = Data
                .Select(x => new { Offer = x, Rooms = (x.Beds + x.Baths), Price = x.Price })
                .Where(x => x.Price == Data.Min(y => y.Price));
            var property = data.FirstOrDefault(x => x.Rooms == data.Max(y => y.Rooms)).Offer;

            FullReport(new [] {property});
        }
        public void ShowAll()
        {
            FullReport(Data);
        }

        private void ShortReport(IList<ShortOfferModel> data)
        {
            Console.Clear();
            var table = new Table();
            table.SetHeaders(GetHeaders<ShortOfferModel>());

            foreach (var offer in data)
            {
                table.AddRow
                (
                    offer.Street,
                    offer.City,
                    offer.State,
                    offer.Beds.ToString(),
                    offer.Baths.ToString(),
                    offer.Area.ToString(customCulture),
                    offer.Price.ToString("C", customCulture),
                    offer.GrossPrice.ToString("C", customCulture)
                );
            }
            
            Console.WriteLine(table.ToString());
        }
        private void FullReport(IList<OfferModel> data)
        {
            Console.Clear();
            var table = new Table();
            table.SetHeaders(GetHeaders<OfferModel>());

            foreach (var offer in data)
            {
                table.AddRow
                (
                    offer.Street,
                    offer.City,
                    offer.ZipCode,
                    offer.StateShort,
                    offer.Beds.ToString(),
                    offer.Baths.ToString(),
                    offer.Area.ToString(customCulture),
                    offer.PropertyType,
                    offer.SaleDate.ToShortDateString(),
                    offer.Price.ToString("C", customCulture)
                );
            }
            
            Console.WriteLine(table.ToString());
        }

        private string[] GetHeaders<T>()
        {
            var props = typeof(T).GetProperties();
            return props.Select(x =>
            {
                if (x.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault() is DisplayNameAttribute displayName)
                {
                    return displayName.DisplayName;
                }
                else
                {
                    return null;
                }
            }).Where(x => !string.IsNullOrEmpty(x)).ToArray();
        }

        private IEnumerable<ShortOfferModel> GroupedResidentialByCity()
        {
            return Data.Where(x => x.PropertyType.Equals(PropertyType.Residential.GetDescription()))
                .GroupBy(x => x.City)
                .Select(x => new ShortOfferModel
                {
                    Id = x.First().Id,
                    City = x.First().City,
                    Area = x.First().Area,
                    Price = x.First().Price,
                    State = x.First().StateShort,
                    Baths = x.First().Baths,
                    Beds = x.First().Beds
                });
        }

        private void SetPathToFile()
        {
            while (true)
            {
                Console.WriteLine("Please enter CSV data source path: ");
                selectedFile = Console.ReadLine();
                if (File.Exists(selectedFile))
                {
                    break;
                }
                else
                {
                    Console.Clear();
                    Console.Error.WriteLine("File not exists!");
                }
            }
        }
    }

}