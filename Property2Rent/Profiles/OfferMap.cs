﻿using System.Globalization;
using CsvHelper.Configuration;
using Property2Rent.Models;

namespace Property2Rent.Profiles
{
    public sealed class OfferMap : ClassMap<OfferModel>
    {
        public OfferMap()
        {
            AutoMap(CultureInfo.InvariantCulture);
            Map(m => m.Id).Ignore();
        }
    }
}