﻿using AutoMapper;
using Property2Rent.Models;

namespace Property2Rent.Profiles
{
    public class OfferProfile : Profile
    {
        public OfferProfile()
        {
            CreateMap<OfferModel, ShortOfferModel>()
                .ForMember(dest => dest.Area, opt => opt.MapFrom(x => x.Area))
                .ForMember(dest => dest.Baths, opt => opt.MapFrom(x => x.Baths))
                .ForMember(dest => dest.Beds, opt => opt.MapFrom(x => x.Beds))
                .ForMember(dest => dest.City, opt => opt.MapFrom(x => x.City))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(x => x.Street))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(x => x.Price))
                .ForMember(dest => dest.State, opt => opt.MapFrom(x => x.StateShort));
        }
    }
}