﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Property2Rent.Utils
{
    public class DataTransfer
    {
        private static DataTransfer instance = null;
        private readonly HttpClient http;
        public static DataTransfer Instance = instance ?? new DataTransfer();

        private DataTransfer()
        {
            http = new HttpClient();
        }

        public async Task<string> GetTax(string city)
        {
            var uri = new Uri($"http://net-poland-interview-stretto.us-east-2.elasticbeanstalk.com/api/flats/taxes?city={city}");
            return await GetDataAsString(uri);
        }

        public async Task<string> GetCsvData()
        {
            var uri = new Uri("http://net-poland-interview-stretto.us-east-2.elasticbeanstalk.com/api/flats/csv");
            return await GetDataAsString(uri);
        }

        private async Task<string> GetDataAsString(Uri uri)
        {
            var response = await http.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }

            throw new HttpRequestException($"Server response: {response.StatusCode.ToString()}");
        }
    }
}