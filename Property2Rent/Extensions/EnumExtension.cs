﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace Property2Rent.Extensions
{
    public static class EnumExtension
    {
        public static string GetDescription<T>(this T p) where T : IConvertible
        {
            if (p is Enum)
            {
                var type = p.GetType();
                var values = Enum.GetValues(type);

                foreach (int item in values)
                {
                    if (item == p.ToInt32(CultureInfo.InvariantCulture))
                    {
                        var info = type.GetMember(type.GetEnumName(item) ?? string.Empty);

                        if (info[0]
                                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                                .FirstOrDefault() is DescriptionAttribute description)
                        {
                            return description.Description ?? string.Empty;
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException("Bad argument. You can use it only with Enums");
            }

            return string.Empty;
        }
        
        public static Array ToArray<T>() where T : IConvertible
        {
            var type = typeof(T);
            if (!type.IsEnum)
            {
                throw new ArgumentException("Bad argument. You can use it only with Enums");
            }
            return Enum.GetValues(type);
        }
    }
}