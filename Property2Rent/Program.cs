﻿using System;
using AutoMapper;
using Property2Rent.Models;
using Property2Rent.Profiles;
using Property2Rent.Utils;

namespace Property2Rent
{
    class Program
    {
        public static void Main(string[] args)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<OfferProfile>();
            });
            var mapper = new Mapper(config);
            var offer = new OfferParser.OfferParser(mapper);
            Console.Clear();
            offer.LoadData();
            var exit = false;
            var menu = PrepareMenu();
            
            while (!exit)
            {
                Console.Clear();
                Console.WriteLine(menu.ToString());
                Console.Write("Please select action: ");
                var option = Console.ReadKey();
                Console.Clear();
                switch (option.Key)
                {
                    case ConsoleKey.A:
                        offer.ShowAll();
                        break;
                    case ConsoleKey.R:
                        offer.BiggestResidential();
                        break;
                    case ConsoleKey.C:
                        offer.CheapestProperties();
                        break;
                    case ConsoleKey.E:
                        offer.MostExpensiveResidential();
                        break;
                    case ConsoleKey.X:
                        exit = true;
                    break;
                }

                if (exit) continue;
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
        }

        private static Table PrepareMenu()
        {
            var navigation =
                new[]
                {
                    new MenuModel { Description = "All offers", ActionKey = "A" },
                    new MenuModel { Description = "Biggest residential", ActionKey = "R" },
                    new MenuModel { Description = "Cheapest property", ActionKey = "C" },
                    new MenuModel { Description = "Most expensive", ActionKey = "E" },
                    new MenuModel { Description = "Exit", ActionKey = "X" }
                };
            
            var menu = new Table();
            menu.SetHeaders("Option", "Action key");
            foreach (var item in navigation)
            {
                menu.AddRow(item.Description, item.ActionKey);
            }

            return menu;
        }
    }
}