﻿using System.ComponentModel;

namespace Property2Rent.Enums
{
    public enum PropertyType
    {
        [Description("Residential")]
        Residential,
        [Description("Comertial Apartment")]
        Condo,
        [Description("Multi-Family")]
        MultiFamily,
        [Description("Investment")]
        Investment
    }
}