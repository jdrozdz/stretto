﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using CsvHelper.Configuration.Attributes;

namespace Property2Rent.Models
{
    public class OfferModel
    {
        private DateTime saleDate;

        public Guid Id { get; set; } = Guid.NewGuid();
        
        [RegularExpression(@"^\d{4}(?:[\s]\S{5,})?$", ErrorMessage = "Incorrect address")]
        [Name("street")]
        [DisplayName("Street")]
        public string Street { get; set; }
        
        [Name("city")]
        [DisplayName("City")]
        public string City { get; set; }
        
        [RegularExpression(@"^\d{5}(?:[-\s]\d{4})?$", ErrorMessage = "Zip Code is incorrect")]
        [Name("zip")]
        [DisplayName("Zip code")]
        public string ZipCode { get; set; }
        
        [MaxLength(3, ErrorMessage = "Shortcut of state can not be longer than 3 characters")]
        [Name("state")]
        [DisplayName("State")]
        public string StateShort { get; set; }

        [MinLength(1, ErrorMessage = "Property does not meet the requirements!")]
        [Name("beds")]
        [DisplayName("Beds")]
        public int Beds { get; set; }

        [MinLength(1, ErrorMessage = "Property does not meet the requirements!")]
        [Name("baths")]
        [DisplayName("Baths")]
        public int Baths { get; set; }
        
        [Name("sq__ft")]
        [DisplayName("SQFT")]
        public double Area { get; set; }
        
        [Name("type")]
        [DisplayName("Type")]
        public string PropertyType { get; set; }
        
        [DisplayName("Sale Date")]
        public DateTime SaleDate
        {
            get => saleDate;
        }
        
        [Name("sale_date")]
        public string SetSaleDate
        {
            set => saleDate = DateTime.ParseExact(value.ToString(), "ddd MMM dd hh:mm:ss EDT yyyy", CultureInfo.InvariantCulture);
        }

        [Name("price")]
        [DisplayName("Price")]
        public double Price { get; set; }

        [Name("latitude")]
        public double Latitude { get; set; }
        
        [Name("longitude")]
        public double Longitude { get; set; }
    }
}