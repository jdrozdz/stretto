﻿using System;
using System.ComponentModel;
using System.Globalization;
using Property2Rent.Utils;

namespace Property2Rent.Models
{
    public class ShortOfferModel
    {
        public Guid Id { get; set; }
        
        [DisplayName("Street")]
        public string Street { get; set; }
        
        [DisplayName("City")]
        public string City { get; set; }
        
        [DisplayName("State")]
        public string State { get; set; }

        [DisplayName("Baths")]
        public int Baths { get; set; }
        
        [DisplayName("Beds")]
        public int Beds { get; set; }
        
        [DisplayName("SQFT")]
        public double Area { get; set; }
        
        [DisplayName("Net Price")]
        public double Price { get; set; }

        public double Tax
        {
            get
            {
                var taxValue = DataTransfer.Instance.GetTax(City).Result;
                return double.TryParse(taxValue, NumberStyles.Number, CultureInfo.CreateSpecificCulture ("en-US"), out var tax) ? tax : 0.00;
            }
        }

        [DisplayName("Gross price")]
        public double GrossPrice => (Price + Price * Tax);
    }
}